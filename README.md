# Logistics Portal

### Test Users

email : test@gmail.com <br>
pass : password <br>
type : NON-ADMIN

email : admin@gmail.com <br>
pass : password <br>
type : ADMIN

- copy contents of envexp along with values to a new file called **.env**
- run files schema.sql and dump_data.sql in your MySQL Workbench
- _npm start_ to start server

## Users

- Category Head – Each category for revels should have one login associated with their email
- Admin – One admin user to fulfil requests.

## Functionalities

1. Categories should be able to request an item from a given set of items. An items request includes the item itself and the quantity.
2. For an item that does not exist in the given list of available items, categories should be able to generate a different type of request for a new item, describing the new item ( name , description )
3. Status information of all the requests should be given to the categories.
4. Admins should be able to see all requests ( from which category )
5. Admins should be able to set the given list of items and quantity they have available. They can alter the list and the items ( remove, add items or modify quantity )
6. Admins can approve requests or decline and provide a reason.
7. Upon approval of request of first type ( described by 1 ), the inventory quantity should be subtracted by the request quantity.
8. Upon approval of request of any type of request, along with status update on the portal, an email should be sent to the category email who has requested it.
