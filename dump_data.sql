insert into `rev20`.`log_cats` (`cat_name`, `cat_email`, `cat_pass`, `cat_type`) values ('Informals', 'test@gmail.com', '$2y$10$MvSStT8mulz6HVqATLBWdeWsmVA9KhLbFLk/wD8Aic3ybjUAJy4za', 'NON-ADMIN');
insert into `rev20`.`log_cats` (`cat_name`, `cat_email`, `cat_pass`, `cat_type`) values ('Logistics', 'admin@gmail.com', '$2y$10$drDEwOrV/BXPI2uM4xKYZ.X.by739z5reNCy3BIBw/A5PmyeMCQtS', 'ADMIN');

insert into `rev20`.`log_items` (`item_name`, `item_q`) values ('Scissors', '10');
insert into `rev20`.`log_items` (`item_name`, `item_q`) values ('Chart Paper', '20');
insert into `rev20`.`log_items` (`item_name`, `item_q`) values ('Tape', '50');
insert into `rev20`.`log_items` (`item_name`, `item_q`) values ('Glue Gun', '10');


insert into rev20.log_cats (`cat_name`, `cat_email`, `cat_pass`, `cat_type`) values ('Sponsorship', 'test@gmail.com', '$2y$10$TVdbt5/pHHJAuP7kbmEhRuXuYWBSMaj00ogaOl64S098aM/aSnOIW', 'NON-ADMIN');