use rev20;
drop table log_reqs;
create table `log_reqs` (
    `req_id` int(11) not null primary key auto_increment,
    `req_date` date not null,
    `cat_id` int(11),
    `item_id` int(11),
    `req_q` int not null,
    `req_status` ENUM('PENDING', 'ACCEPTED', 'REJECTED') not null default 'PENDING',
    `req_reason` text,
    foreign key (cat_id) references log_cats(cat_id) on delete cascade on update cascade,
    foreign key (item_id) references log_items(item_id) on delete cascade on update cascade
);
update log_cats set cat_email='<YOUR EMAIL>' where cat_name=1;