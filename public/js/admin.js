function filter() {
  var filterValue, input, ul, li, item;
  input = document.getElementById("input_item");
  filterValue = input.value.toUpperCase();
  ul = document.getElementById("search-list");
  li = ul.getElementsByTagName("li");
  for (var i = 0; i < li.length; i++) {
    item = li[i].getElementsByClassName("list-item")[0];
    if (filterValue === "") {
      item.className = "list-item";
    } else if (item.innerHTML.toUpperCase().indexOf(filterValue) > -1) {
      item.className = "list-item";
    } else if (item.innerHTML.toUpperCase().indexOf(filterValue) <= -1) {
      item.className = "list-item hidden";
    }
  }
}

function filterReqs() {
  let selectedCat, id;
  let dropDown = document.getElementById("cat-drop");
  selectedCat = dropDown.options[dropDown.selectedIndex].value;
  //console.log(selectedCat);

  let reqs = document.getElementsByClassName("list-item");
  for (var i = 0; i < reqs.length; i++) {
    if (selectedCat === "all") {
      reqs[i].style.display = "flex";
    } else if (
      selectedCat ===
      reqs[i]
        .getElementsByClassName("cat-name")[0]
        .getElementsByClassName("content")[0]
        .innerHTML.trim()
    ) {
      reqs[i].style.display = "flex";
      // if(document.getElementsByName("p-requests")[0].style.display === "block"){
      //   reqs[i].className = "list-item pending";
      // } else {
      //   if(reqs[i].className.slice(10) === "pending"){
      //     reqs[i].className = "list-item pending";
      //   } else if(reqs[i].className.slice(10) === "accepted"){
      //     reqs[i].className = "list-item accepted";
      //   } else if(reqs[i].className.slice(10) === "rejected"){
      //     reqs[i].className = "list-item rejected";
      //   }
      // }
    } else {
      reqs[i].style.display = "none";
    }
  }
}

// --------------------------------------------------

function showModal(id) {
  let list_item = document.getElementById(id.substring(3));
  let modal = document.getElementById("modal");
  let updateModal = document.getElementById("modal-update");
  let deleteModal = document.getElementById("modal-delete");

  let modal_id = document.getElementById("modal-id");
  let modal_name = document.getElementById("modal-name");
  let modal_input = document.getElementById("update-q");

  modal_id.innerHTML = list_item
    .getElementsByClassName("id")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modal_name.innerHTML = list_item
    .getElementsByClassName("name")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modal_input.placeholder = list_item
    .getElementsByClassName("q")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modal.style.display = "block";
  updateModal.style.display = "flex";
  deleteModal.style.display = "none";
}

function closeModal() {
  let modal = document.getElementById("modal");
  modal.style.display = "none";
}

function showDelete(id) {
  let list_item = document.getElementById(id.substring(5));
  let modal = document.getElementById("modal");
  let updateModal = document.getElementById("modal-update");
  let deleteModal = document.getElementById("modal-delete");

  let modal_id = document.getElementById("modal-id-d");
  let modal_name = document.getElementById("modal-name-d");
  let modal_q = document.getElementById("modal-q-d");

  modal_id.innerHTML = list_item
    .getElementsByClassName("id")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modal_name.innerHTML = list_item
    .getElementsByClassName("name")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modal_q.innerHTML = list_item
    .getElementsByClassName("q")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modal.style.display = "block";
  updateModal.style.display = "none";
  deleteModal.style.display = "flex";
}

// ---------------------------------------------------

function updateItem() {
  let modal_id = document.getElementById("modal-id");
  let modal_name = document.getElementById("modal-name");
  let modal_input = document.getElementById("update-q");

  if (
    modal_input.value === "" ||
    modal_input.value === undefined ||
    modal_input.value === null ||
    modal_input.value === NaN ||
    modal_input.value < 0
  ) {
    // Validation
    alert("Enter a valid quantity");
  } else {
    ob = {
      item_id: parseInt(modal_id.innerHTML.trim()),
      item_name: modal_name.innerHTML.trim(),
      item_q: parseInt(modal_input.value)
    };
    console.log(ob);
    fetch("/items", {
      method: "PUT",
      headers: {
        "Content-Type": "application/JSON"
      },
      credentials: "include",
      body: JSON.stringify(ob),
      redirect: "error"
    })
      .then(res => {
        return res.json();
      })
      .then(json => {
        if (json.success) {
          window.location.replace("/admin/items");
        } else {
          alert(json.msg);
        }
      })
      .catch(error => {
        alert("Internal Server Error. Contact SysAdm.");
        console.log(error);
      });
  }
}

function deleteItem() {
  let modal_id = document.getElementById("modal-id-d");
  ob = {
    item_id: modal_id.innerHTML.trim()
  };
  console.log(ob);
  fetch("/items/delete", {
    method: "DELETE",
    headers: {
      "Content-Type": "application/JSON"
    },
    credentials: "include",
    body: JSON.stringify(ob),
    redirect: "error"
  })
    .then(res => {
      return res.json();
    })
    .then(json => {
      if (json.success) {
        window.location.replace("/admin/items");
      } else {
        alert(json.msg);
      }
    })
    .catch(error => {
      alert("Internal Server Error. Contact SysAdm.");
      console.log(error);
    });
}

// ------------------------------------------------------------

function showPending() {
  document.getElementsByName("p-requests")[0].style.display = "block";
  document.getElementsByName("a-requests")[0].style.display = "none";
}

function showAll() {
  document.getElementsByName("p-requests")[0].style.display = "none";
  document.getElementsByName("a-requests")[0].style.display = "block";
}

// ------------------------------------------------------------

function showAccept(id) {
  let modal = document.getElementById("modal");
  let rejectModal = document.getElementById("reject-modal");
  let acceptModal = document.getElementById("accept-modal");

  let listItem = document.getElementById(id.substring(5));
  let modalCat = document.getElementById("modal-cat");
  let modalItem = document.getElementById("modal-item");
  let modalQ = document.getElementById("modal-q");
  let modalId = document.getElementById("modal-id");
  let modalItemId = document.getElementById("modal-item_id");
  let modalItemQ = document.getElementById("modal-item_q");
  let modalCatEmail = document.getElementById("modal-cat_email");
  let modalReqDate = document.getElementById("modal-req_date");

  modalCat.innerHTML = listItem
    .getElementsByClassName("cat-name")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalItem.innerHTML = listItem
    .getElementsByClassName("item-name")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalQ.innerHTML = listItem
    .getElementsByClassName("item_q")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalId.innerHTML = listItem
    .getElementsByClassName("id")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalReqDate.innerHTML = listItem
    .getElementsByClassName("date")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalItemId.innerHTML = listItem.getElementsByClassName("item-id")[0].id;
  modalItemQ.innerHTML = listItem.getElementsByClassName("item-q")[0].id;
  modalCatEmail.innerHTML = listItem.getElementsByClassName("cat_email")[0].id;

  modal.style.display = "block";
  rejectModal.style.display = "none";
  acceptModal.style.display = "flex";
}

function showReject(id) {
  let modal = document.getElementById("modal");
  let rejectModal = document.getElementById("reject-modal");
  let acceptModal = document.getElementById("accept-modal");

  let listItem = document.getElementById(id.substring(5));
  let modalCat = document.getElementById("modal-cat-r");
  let modalItem = document.getElementById("modal-item-r");
  let modalQ = document.getElementById("modal-q-r");
  let modalId = document.getElementById("modal-id-r");
  let modalItemId = document.getElementById("modal-item_id-r");
  let modalItemQ = document.getElementById("modal-item_q-r");
  let modalCatEmail = document.getElementById("modal-cat_email-r");
  let modalReqDate = document.getElementById("modal-req_date-r");

  modalCat.innerHTML = listItem
    .getElementsByClassName("cat-name")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalItem.innerHTML = listItem
    .getElementsByClassName("item-name")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalQ.innerHTML = listItem
    .getElementsByClassName("item_q")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalId.innerHTML = listItem
    .getElementsByClassName("id")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalReqDate.innerHTML = listItem
    .getElementsByClassName("date")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  modalItemId.innerHTML = listItem.getElementsByClassName("item-id")[0].id;
  modalItemQ.innerHTML = listItem.getElementsByClassName("item-q")[0].id;
  modalCatEmail.innerHTML = listItem.getElementsByClassName("cat_email")[0].id;

  modal.style.display = "block";
  rejectModal.style.display = "flex";
  acceptModal.style.display = "none";
}

// -------------------------------------------------------

function acceptReq() {
  let req_id = document.getElementById("modal-id");
  let req_q = document.getElementById("modal-q");
  let item_id = document.getElementById("modal-item_id");
  let item_q = document.getElementById("modal-item_q");
  let cat_email = document.getElementById("modal-cat_email");
  let item_name = document.getElementById("modal-item");
  let req_date = document.getElementById("modal-req_date");

  let ob = {
    req_id: parseInt(req_id.innerHTML.trim()),
    req_q: parseInt(req_q.innerHTML.trim()),
    item_id: parseInt(item_id.innerHTML.trim()),
    item_q: parseInt(item_q.innerHTML.trim()),
    cat_email: cat_email.innerHTML.trim(),
    item_name: item_name.innerHTML.trim(),
    req_date: req_date.innerHTML.trim()
  };

  // console.log(ob);

  fetch("/requests/accept", {
    method: "PUT",
    headers: {
      "Content-Type": "application/JSON"
    },
    credentials: "include",
    body: JSON.stringify(ob),
    redirect: "error"
  })
    .then(res => {
      return res.json();
    })
    .then(json => {
      if (json.success) {
        window.location.replace("/admin/requests");
      } else {
        alert(json.msg);
      }
    })
    .catch(error => {
      alert("Internal Server Error. Contact System Admin");
      console.log(error);
    });
}

// -------------------------------------------------------------

function rejectReq() {
  let req_id = document.getElementById("modal-id-r");
  let req_q = document.getElementById("modal-q-r");
  let item_id = document.getElementById("modal-item_id-r");
  let req_reason = document.getElementById("req-reason");
  let item_q = document.getElementById("modal-item_q-r");
  let cat_email = document.getElementById("modal-cat_email-r");
  let item_name = document.getElementById("modal-item-r");
  let req_date = document.getElementById("modal-req_date-r");

  let ob = {
    req_id: parseInt(req_id.innerHTML.trim()),
    req_q: parseInt(req_q.innerHTML.trim()),
    item_id: parseInt(item_id.innerHTML.trim()),
    item_q: parseInt(item_q.innerHTML.trim()),
    item_name: item_name.innerHTML.trim(),
    req_reason: req_reason.value.trim(),
    cat_email: cat_email.innerHTML.trim(),
    req_date: req_date.innerHTML.trim()
  };

  //console.log(ob);
  fetch("/requests/reject", {
    method: "PUT",
    headers: {
      "Content-Type": "application/JSON"
    },
    credentials: "include",
    body: JSON.stringify(ob),
    redirect: "error"
  })
    .then(res => {
      return res.json();
    })
    .then(json => {
      if (json.success) {
        window.location.replace("/admin/requests");
      } else {
        alert(json.msg);
      }
    })
    .catch(error => {
      alert("Internal Server Error. Contact System Admin");
      console.log(error);
    });
}

function returnItem(id) {
  let req_id = id.substring(10);
  let listItem = document.getElementById(req_id);

  let req_q = listItem
    .getElementsByClassName("item_q")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();
  let item_name = listItem
    .getElementsByClassName("item-name")[0]
    .getElementsByClassName("content")[0]
    .innerHTML.trim();

  let ob = {
    req_id,
    req_q,
    item_name
  };
  console.log(ob);

  fetch("/requests/return", {
    method: "PUT",
    headers: {
      "Content-Type": "application/JSON"
    },
    credentials: "include",
    body: JSON.stringify(ob),
    redirect: "error"
  })
    .then(res => {
      return res.json();
    })
    .then(json => {
      if (json.success) {
        window.location.replace("/admin/requests");
      } else {
        alert(json.msg);
      }
    })
    .catch(error => {
      alert("Internal Server Error. Contact System Admin");
      console.log(error);
    });
}
