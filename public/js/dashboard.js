console.log("in script");

function filter() {
  var filterValue, input, ul, li, item;
  input = document.getElementById("input-item");
  filterValue = input.value.toUpperCase();
  ul = document.getElementById("search-list");
  li = ul.getElementsByTagName("li");
  for (i = 0; i < li.length; i++) {
    item = li[i].getElementsByClassName("search-item")[0];
    if (filterValue === "") {
      item.className = "search-item item-hidden";
    } else if (item.innerHTML.toUpperCase().indexOf(filterValue) > -1) {
      item.className = "search-item";
    } else if (item.innerHTML.toUpperCase().indexOf(filterValue) <= -1) {
      item.className = "search-item item-hidden";
    }
  }
}

function update(div) {
  let val = document.getElementById(div.id).textContent.trim();
  document.getElementById("input-item").value = val;
  document.getElementById("input-id").value = div.id;
  //console.log(document.getElementById("input-item").value);
}

function testSubmit() {
  let item = document.getElementById("input-item").value;
  let q = document.getElementById("input-q").value;
  let ul = document.getElementById("search-list");
  let li = ul.getElementsByTagName("li");
  let x;

  let today = new Date();
  let dd = String(today.getDate()).padStart(2, "0");
  let mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  let yyyy = today.getFullYear();

  let finalToday = yyyy + "-" + dd + "-" + mm;

  if (
    item === undefined ||
    item === null ||
    item === "" ||
    q === undefined ||
    q === null ||
    q === "" ||
    q <= 0 ||
    isNaN(q)
  ) {
    alert("Enter values");
  } else {
    let id = null;
    for (var i = 0; i < li.length; i++) {
      x = li[i].getElementsByClassName("search-item")[0];
      if (x.textContent.trim().toUpperCase() === item.toUpperCase()) {
        id = x.id;
        break;
      }
      x = undefined; // to reset value
    }
    let ob = {
      item_id: id,
      item_name: x === undefined ? item : x.textContent.trim(),
      item_q: q,
      item_exists: x === undefined ? 0 : 1,
      req_date: finalToday
    };

    fetch("/request", {
      method: "POST",
      headers: {
        "Content-Type": "application/JSON"
      },
      credentials: "include",
      body: JSON.stringify(ob),
      redirect: "error"
    })
      .then(res => {
        return res.json();
      })
      .then(json => {
        if (json.success) {
          window.location.replace("/dashboard");
        } else {
          alert(json.msg);
        }
      })
      .catch(error => {
        alert("Internal Server Error. Contact SysAdm.");
        console.log(error);
      });

    console.log(ob);
    console.log(finalToday);
    document.getElementById("input-item").value = "";
    document.getElementById("input-q").value = "";
  }

  // let id;
  // for (var i = 0; i < li.length; i++) {
  //     let x = li[i].getElementsByClassName("search-item")[0];
  //     if (x.textContent.trim() === item) {
  //         id = x.id;
  //         break;
  //     }
  // }
  // console.log(id);
}

function cancel(id) {
  id=parseInt(id);
  let obId = { 
    req_id: id
  };
  console.log(id, obId);
  fetch("/requests/cancel", {
    method: "DELETE",
    headers: {
      "Content-Type": "application/JSON"
    },
    credentials: "include",
    body: JSON.stringify(obId),
    redirect: "error"
  })
    .then(res => {
      return res.json();
    })
    .then(json => {
      if (json.success) {
        window.location.replace("/dashboard");
      } else {
        alert(json.msg);
      }
    })
    .catch(error => {
      alert("Internal Server Error. Contact SysAdm.");
      console.log(error);
    });
}

