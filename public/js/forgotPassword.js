function submitEmail() {
  let email = document.getElementsByName("email")[0].value;
  ob = {
    email: email
  };
  fetch("/forgotpassword",{
    method:"POST",
    headers :{
      "Content-Type":"application/JSON"
    },
    credentials:"include",
    body: JSON.stringify(ob),
    redirect:"error"
  })
    .then(res=>{
      return res.json();
    })
    .then(json=>{
      if(json.success){
        window.alert("The email has been sent to the submitted email address");
      }
      else{
        alert(json.msg);
      }  
    })
    .catch(error=>{
      alert("Internal Server Error. Contact Sys Admin");
      console.log(error);
    });
    console.log(ob);
    document.getElementsByName("email")[0].value="";

}

function resetPassword() {
  let pass1 = document.getElementsByName("password1")[0].value;
  let pass2 = document.getElementsByName("password2")[0].value;

  if (pass1 !== pass2) {
    alert("Passwords don't match");
  } else {
    let id = document.getElementsByName("id")[0].value;
    let token = document.getElementsByName("token")[0].value;
    let new_pass = document.getElementsByName("password1")[0].value;
    let ob = {
      id: id,
      token: token,
      new_pass: new_pass
    };
    console.log(ob);
    fetch("/resetpassword",{
      method:"PUT",
      headers :{
        "Content-Type":"application/JSON"
      },
      credentials:"include",
      body: JSON.stringify(ob),
      redirect:"error"
    })
      .then(res=>{
        return res.json();
      })
      .then(json=>{
        if(json.success){
          window.alert("The Password has been reset.");
        }
        else{
          alert(json.msg);
        }  
      })
      .catch(error=>{
        alert("Internal Server Error. Contact Sys Admin");
        //console.log(error);
      });

    console.log(ob);
  }
}
