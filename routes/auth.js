const db = require("../config/db");
const to = require("../utils/to");
const bcrypt = require("bcryptjs");

module.exports = {
  login: async function (req, res) {
    try {
      let cat_email = req.body.cat_email;
      let cat_pass = req.body.cat_pass;

      let err, result, cat_query;

      cat_email = String(cat_email)
        .trim()
        .toLowerCase();
      cat_pass = String(cat_pass).trim();

      let prom = (await db).query(
        "SELECT * FROM log_cats WHERE cat_email = ?",
        [cat_email]
      );
      [err, result] = await to(prom);

      if (err) {

        return res.sendError(null);
      } else {
        if (result.length <= 0) {
          return res.sendError(null, "Invalid Category Credentials");
        } else {
          let pass = result[0].cat_pass,
            err_bcrypt,
            res_bcrypt;

          [err_bcrypt, res_bcrypt] = await to(bcrypt.compare(cat_pass, pass));
          if (err_bcrypt) {
            return res.sendError(null);
          } else if (!res_bcrypt) {
            return res.sendError(null, "Invalid Category credentials");
          } else {
            cat_query = result[0];
            req.session.logged_in = true;
            req.session.cat_id = result[0].cat_id;
            req.session.cat_email = result[0].cat_email;
            req.session.cat_name = result[0].cat_name;
            req.session.cat_type = result[0].cat_type;
            req.session.save();
            return res.sendSuccess(null, "Logged In");
          }
        }
      }
    } catch (err) {
      console.log(err);
      return res.sendError(null);
    }
  },
  logout: async function (req, res) {
    try {
      req.session.destroy(err => {
        if (err) console.log(err);
        return res.redirect("/");
      });
    } catch (err) {
      console.log(err);
      return res.redirect("/");
    }
  }
};