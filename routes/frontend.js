const path = require("path");
const db = require("../config/db");
const to = require("../utils/to");
const jwt = require("jwt-simple");

module.exports = {
  login: function(req, res) {
    return res.render("index");
  },

  forgotPassword: function(req, res) {
    return res.render("forgotpassword");
  },

  resetPassword: async function(req, res) {
    // TODO: Make secret, get payload from token&secret
    let secret;
    let user;
    let getOb = (await db).query("SELECT * FROM log_cats WHERE cat_id=?", [
      req.params.id
    ]);
    [err, result] = await to(getOb);
    if (err) console.log(err);
    else {
      user = {
        cat_id: result[0].cat_id,
        cat_name: result[0].cat_name,
        cat_pass: result[0].cat_pass
      };
    }
    secret = user.cat_name + "-" + user.cat_pass;
    console.log(user);
    let payload = jwt.decode(req.params.token, secret);

    let load = {
      id: payload.id,
      token: req.params.token
    };

    // Send load along with render
    return res.render("resetpassword", {
      load
    });
  },

  dashboard: async function(req, res) {
    let items = [];
    let requests = [];
    let err, result;
    let prom = (await db).query("SELECT * from log_items");
    [err, result] = await to(prom);

    if (err) {
      res.sendError(null);
    } else {
      for (var i = 0; i < result.length; i++) {
        items[i] = {
          item_id: result[i].item_id,
          item_name: result[i].item_name
        };
      }
      console.log("Items loaded");
    }

    let get_reqs = (
      await db
    ).query(
      "SELECT req_id, req_date, EXTRACT(YEAR FROM req_date) AS year,MONTHNAME(req_date) AS month,DAYNAME(req_date) as dayname,EXTRACT(DAY FROM req_date) as day, cat_id, r.item_id, i.item_name, req_q, req_status, req_reason FROM log_reqs r INNER JOIN log_items i ON r.item_id=i.item_id WHERE cat_id=? ORDER BY req_id DESC",
      [req.session.cat_id]
    );

    [err, result] = await to(get_reqs);

    if (err) throw err;
    else {
      for (var i = 0; i < result.length; i++) {
        requests[i] = {
          req_id: result[i].req_id,
          req_date:
            result[i].dayname +
            ", " +
            result[i].day +
            " " +
            result[i].month +
            " " +
            result[i].year,
          cat_id: result[i].cat_id,
          item_id: result[i].item_id,
          item_name: result[i].item_name,
          req_q: result[i].req_q,
          req_status: result[i].req_status,
          req_reason: result[i].req_reason
        };
      }
      console.log("Request Loaded");
    }

    return res.render("dashboard", {
      items,
      requests,
      cat_name: req.session.cat_name
    });
  },

  admin: function(req, res) {
    return res.render("admin");
  },

  admin_items: async function(req, res) {
    let items = [];
    let err, result;
    let prom = (await db).query(
      "SELECT * from log_items ORDER BY item_id DESC"
    );
    [err, result] = await to(prom);

    if (err) {
      res.sendError(null);
    } else {
      for (var i = 0; i < result.length; i++) {
        items[i] = {
          item_id: result[i].item_id,
          item_name: result[i].item_name,
          item_q: result[i].item_q
        };
      }
      console.log("Items Loaded");
    }

    return res.render("items", {
      items
    });
  },

  admin_reqs: async function(req, res) {
    let p_requests = [];
    let a_requests = [];
    let categories = [];
    let err_p, err_a, p_result, a_result;
    // let pending = (await db).query(
    //   "SELECT req_id, req_date,EXTRACT(YEAR FROM req_date) AS year,MONTHNAME(req_date) AS month,DAYNAME(req_date) as dayname,EXTRACT(DAY FROM req_date) as day, cat_name, item_name, req_status, cat_email, req_q, r.item_id from log_reqs r inner join log_cats c on r.cat_id=c.cat_id inner join log_items i on r.item_id=i.item_id WHERE req_status='PENDING' ORDER BY req_id DESC"
    // );
    let all = (await db).query(
      "SELECT req_id, req_date,EXTRACT(YEAR FROM req_date) AS year,MONTHNAME(req_date) AS month,DAYNAME(req_date) as dayname,EXTRACT(DAY FROM req_date) as day, cat_name, item_name, req_status, cat_email, req_q, req_reason, r.item_id, i.item_q, req_status, c.cat_email from log_reqs r inner join log_cats c on r.cat_id=c.cat_id inner join log_items i on r.item_id=i.item_id ORDER BY req_id DESC"
    );
    //[err_p, p_result] = await to(pending);
    [err_a, a_result] = await to(all);

    let cats = (await db).query("SELECT * FROM log_cats");
    [err_c, c_result] = await to(cats);

    if (err_a || err_c) {
      //console.log("here");
      res.sendError(null);
    } else {
      // ALL REQUESTS
      for (var i = 0; i < a_result.length; i++) {
        // Extracting date
        a_result[i].req_date =
          a_result[i].dayname +
          ", " +
          a_result[i].day +
          " " +
          a_result[i].month +
          " " +
          a_result[i].year;

        if (a_result[i].req_status !== "PENDING") {
          a_requests.push(a_result[i]);
        } else {
          p_requests.push(a_result[i]);
          a_requests.push(a_result[i]);
        }
      }

      for (var i = 0; i < c_result.length; i++) {
        let cat = {
          cat_id: c_result[i].cat_id,
          cat_name: c_result[i].cat_name
        };
        categories.push(cat);
      }

      console.log("Requests Loaded");
      return res.render("requests", {
        categories,
        p_requests,
        a_requests
      });
    }
  }
};
