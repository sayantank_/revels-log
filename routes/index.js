var express = require("express");
var router = express.Router();
const frontend = require("./frontend");
const auth = require("./auth");
const request = require("./request");
const items = require("./items");
const password = require("./password");

function notLoggedIn(req, res, next) {
  if (
    typeof req.session.logged_in === "undefined" ||
    req.session.logged_in === false
  )
    return next();
  else return res.redirect("/dashboard");
}

function loggedIn(req, res, next) {
  if (
    typeof req.session.logged_in === "undefined" ||
    req.session.logged_in === false
  )
    return res.redirect("/");
  else return next();
}

function isAdmin(req, res, next) {
  if (req.session.cat_type === "NON-ADMIN") return res.redirect("/dashboard");
  else if (req.session.cat_type === "ADMIN") return next();
  else return res.redirect("/");
}

function isNonAdmin(req, res, next) {
  if (req.session.cat_type === "NON-ADMIN") return next();
  else if (req.session.cat_type === "ADMIN") return res.redirect("/admin");
  else return res.redirect("/");
}

router.get("/", notLoggedIn, frontend.login);

router.post("/login", auth.login);
router.get("/logout", auth.logout);

router.get("/forgotpassword", frontend.forgotPassword);
router.post("/forgotpassword", password.emailSubmit);
router.get("/resetpassword/:id/:token", frontend.resetPassword);
router.put("/resetpassword/", password.passwordReset);

router.post("/request", request.add);
router.put("/requests/accept", request.accept);
router.put("/requests/reject", request.reject);
router.delete("/requests/cancel",request.cancel);
router.put("/requests/return",items.return);

router.put("/items", items.update);
router.delete("/items/delete", items.delete);

router.get("/dashboard", loggedIn, isNonAdmin, frontend.dashboard);

router.get("/admin", loggedIn, isAdmin, frontend.admin);
router.get("/admin/items", loggedIn, isAdmin, frontend.admin_items);
router.get("/admin/requests", loggedIn, isAdmin, frontend.admin_reqs);
router.put("/admin/return", items.return);

module.exports = router;
