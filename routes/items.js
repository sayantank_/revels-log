const db = require("../config/db.js");
const to = require("../utils/to.js");
module.exports = {
  update: async function(req, res) {
    let item_id = req.body.item_id;
    let item_q = req.body.item_q;
    let updating = (
      await db
    ).query("UPDATE log_items SET item_q=? WHERE item_id=?", [item_q, item_id]);
    let err, result;
    [err, result] = await to(updating);
    if (err) throw err;
    else {
      console.log("Item Updated");
      res.sendSuccess(null, "Items updated");
    }
  },
  delete: async function(req, res) {
    let item_id = req.body.item_id;
    console.log(item_id);
    let err, result, err_r, result_r;

    // NOT REQUIRED BECAUSE ON DELETE CASCADE

    // let update_req = (
    //   await db
    // ).query(
    //   "UPDATE log_reqs SET req_status='REJECTED', req_reason='Item Deleted' WHERE item_id=?",
    //   [item_id]
    // );
    // [err_r, result_r] = await to(update_req);

    let delete_item = (await db).query(
      "DELETE FROM log_items WHERE item_id=?",
      [item_id]
    );
    [err, result] = await to(delete_item);

    if (err || err_r) {
      console.log("DELETE ERROR");
      res.sendError(null);
    } else {
      console.log("Item Deleted and Updated Request");
      res.sendSuccess(null, "Item Deleted");
    }
  },

  return: async (req,res) => {
    let reqId = req.body.req_id;
    let reqQ = req.body.req_q;
    let itemName = req.body.item_name;
    console.log(reqQ);
    console.log(itemName)
    let updateItem = (await db).query(
      "UPDATE log_reqs SET req_status = 'RETURNED' WHERE req_id = ?;",
      [reqId]
    );
    [err, result] = await to(updateItem);

    if (err) {
      console.log("RETURN ERROR");
      res.sendError(null);
    } else {
      console.log("Item Returned");
      //res.sendSuccess(null, "Item Returned");
    }
    
    /*let orig_quant;
    
    let quant=(await db).query(
      "SELECT * FROM log_items WHERE item_name=?",
      [itemName]
    );
    [err,result] = await to(quant);
    if(err){
      console.log("Items not found");
      res.sendError(null);
    }
    else
    {
      orig_quant=result[0].item_q;
    }
    console.log(orig_quant);
    */
    let updateQ = (await db).query(
      "UPDATE log_items SET item_q = item_q+? WHERE item_name = ?",
      [reqQ,itemName]
    );
    [err,result] = await to(updateQ);
    if(err){
      console.log("ITEM Q not updated");
      res.sendError(null);
    }
    else {
      console.log("Quat Updated");
      res.sendSuccess(null, "Quant Increased");
    }

  }
};
