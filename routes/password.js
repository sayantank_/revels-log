const db = require("../config/db");
const to = require("../utils/to");
const nodemailer = require("nodemailer");
const ses = require("nodemailer-ses-transport");
const jwt = require("jwt-simple");
const bcrypt = require("bcryptjs")

const transporter = nodemailer.createTransport(
  ses({
    accessKeyId: process.env.AWS_accessKeyId,
    secretAccessKey: process.env.AWS_secretAccessKey
  })
);

const sender = process.env.AWS_verifiedEmail;

module.exports = {
  emailSubmit: async function (req, res) {
    let email = req.body.email;
    let user;
    let getOb = (await db).query("SELECT * FROM log_cats WHERE cat_email=?", [
      email
    ]);
    [err, result] = await to(getOb);
    if (err) throw err;
    else {
      if (result[0] == undefined) {
        return res.sendError(null, "Please enter a valid Email Id.");
      } else {
        user = {
          cat_id: result[0].cat_id,
          cat_name: result[0].cat_name,
          cat_pass: result[0].cat_pass
        };
      }

      console.log(user);
      let payload = {
        id: user.cat_id,
        email: email
      };
      let secret = user.cat_name + '-' + user.cat_pass;
      let token = jwt.encode(payload, secret);
      let output = 'localhost:3000/resetpassword/'+payload.id+'/'+token;
      //let output=
      transporter.sendMail({
        from: sender,
        to: email,
        subject: "Password Reset",
        text: output
      });
      console.log(output);
      return res.sendSuccess(null, "Email sent");
    }
  },

  passwordReset: async function (req, res) {
    let id = req.body.id;
    let hash_pass;
    console.log(req.body);
    let newPass = req.body.new_pass;
    let salt=bcrypt.genSaltSync(10);
    hash_pass=bcrypt.hashSync(newPass,salt);
    console.log(id,newPass,hash_pass);
    let user = (await db).query(
      "UPDATE log_cats SET cat_pass = ? WHERE cat_id = ?",
    [hash_pass,id]
    );
    [err,result] = await to(user);
    if(err) res.sendError("An error occurred. Try again or contact Sys Admin.");
    else{
      console.log('New Password Has Been Set')
    }
    return res.sendSuccess(null,"Password has been set.");
  }
};