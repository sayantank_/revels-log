const db = require("../config/db");
const to = require("../utils/to");
const nodemailer = require("nodemailer");
const ses = require("nodemailer-ses-transport");

const transporter = nodemailer.createTransport(
  ses({
    accessKeyId: process.env.AWS_accessKeyId,
    secretAccessKey: process.env.AWS_secretAccessKey
  })
);

const sender = process.env.AWS_verifiedEmail;

module.exports = {
  add: async function(req, res) {
    if (!req.body.item_exists) {
      let item_id;
      let item_name =
        req.body.item_name.charAt(0).toUpperCase() +
        req.body.item_name.slice(1);

      let item = (await db).query(
        "INSERT INTO log_items VALUES(DEFAULT,?,DEFAULT)",
        item_name
      );
      [err, result] = await to(item);
      if (err) throw err;
      else {
        item_id = result.insertId;
      }
      let reqs = (
        await db
      ).query(
        "INSERT INTO log_reqs VALUES(DEFAULT,STR_TO_DATE(?, '%Y-%d-%m'),?,?,?,?,NULL)",
        [
          req.body.req_date,
          req.session.cat_id,
          item_id,
          req.body.item_q,
          "PENDING"
        ]
      );
      [err, result] = await to(reqs);
      if (err) throw err;
      else {
        console.log("New item created and Request made");
        return res.sendSuccess(null, "Request made");
      }
    } else {
      let reqs = (
        await db
      ).query(
        "INSERT INTO log_reqs VALUES(DEFAULT,STR_TO_DATE(?, '%Y-%d-%m'),?,?,?,?,NULL)",
        [
          req.body.req_date,
          req.session.cat_id,
          req.body.item_id,
          req.body.item_q,
          "PENDING"
        ]
      );
      [err, result] = await to(reqs);
      if (err) throw err;
      else {
        console.log("Request made");
        return res.sendSuccess(null, "Request made");
      }
    }
  },
  accept: async function(req, res) {
    let req_id = req.body.req_id;
    let req_q = req.body.req_q;
    let item_id = req.body.item_id;
    let item_q = req.body.item_q;
    let item_name = req.body.item_name;
    let cat_email = req.body.cat_email;
    let req_date = req.body.req_date;

    let final_item_q = item_q - req_q;

    let err_r, result_r, err_i, result_i;

    let update_req = (
      await db
    ).query("UPDATE log_reqs SET req_status='ACCEPTED' WHERE req_id=?", [
      req_id
    ]);
    [err_r, result_r] = await to(update_req);

    let update_item = (
      await db
    ).query("UPDATE log_items SET item_q=? WHERE item_id=?", [
      final_item_q,
      item_id
    ]);
    [err_i, result_i] = await to(update_item);

    if (err_i || err_r) {
      console.log("ACCEPT ERROR");
      res.sendError(null);
    } else {
      console.log("Request Accepted");
      let output = `Your request has been Accepted.
                    \nDetails for the Request: \n
                    Request Id: ${req_id}\n
                    Request Date: ${req_date}\n
                    Item Name: ${item_name}\n
                    Requested Quantity: ${req_q}\n`;
      transporter.sendMail({
        from: sender,
        to: cat_email,
        subject: "LOGISTICS - Revels'20 | Request Accepted",
        text: output
      });
      console.log("Email sent");
      res.sendSuccess(null, "Request Accepted");
    }

    // let update_req = (
    //   await db
    // ).query("UPDATE log_reqs SET req_status='ACCEPTED' WHERE req_id=?", [
    //   req_id
    // ]);
    // let update_item = (
    //   await db
    // ).query("UPDATE log_items SET item_q=? WHERE item_id=?", [
    //   item_q - req_q,
    //   item_id
    // ]);

    // [err_r, result_r] = await to(update_req);
    // [err_i, result_i] = await to(update_item);

    // if (err_i || err_r) {
    //   console.log("ACCEPT ERROR");
    //   throw err_i ? err_i : err_r;
    // } else {
    //   console.log("Request accepted");
    //   res.sendSuccess(null, "Request Accepted");
    // }
  },
  reject: async function(req, res) {
    let req_id = req.body.req_id;
    let req_q = req.body.req_q;
    let item_id = req.body.item_id;
    let item_q = req.body.item_q;
    let item_name = req.body.item_name;
    let cat_email = req.body.cat_email;
    let req_date = req.body.req_date;
    let req_reason = req.body.req_reason;

    let reject = (
      await db
    ).query("UPDATE log_reqs SET req_status=?, req_reason=? WHERE req_id=?", [
      "REJECTED",
      req_reason,
      req_id
    ]);

    let [err, results] = await to(reject);

    if (err) throw err;
    else {
      console.log("Request rejected");
      output = `Your request has been Rejected.\n
                Details for the request:\n
                Request Id: ${req_id}\n
                Request Date: ${req_date}\n
                Requested Item: ${item_name}\n
                Requested Quantity: ${req_q}\n
                Reason For The Request To Be Rejected: ${req_reason}\n`;
      transporter.sendMail({
        from: sender,
        to: cat_email,
        subject: "LOGISTICS - Revels'20 | Request Rejected",
        text: output
      });
      console.log("Email sent");
      res.sendSuccess(null, "Request Rejected");
    }
  },

  cancel: async function(req, res) {
    let del_id = req.body.req_id;
    let del_req = (await db).query("DELETE FROM log_reqs WHERE req_id = ?", [
      del_id
    ]);
    [err, result] = await to(del_req);
    if (err) return res.sendError(null, "An error occurred");
    else {
      console.log("Request Deleted");
      return res.sendSuccess(null, "Request Deleted");
    }
  }
};
