create schema if not exists `rev20`;

use `rev20`;

drop table if exists `log_cats`;
create table `log_cats` (
    `cat_id` int(11) not null primary key auto_increment,
    `cat_name` varchar(256) not null,
    `cat_email` varchar(256) not null unique,
    `cat_pass` varchar(256) not null,
    `cat_type` ENUM('ADMIN', 'NON-ADMIN') not null default 'NON-ADMIN'
);

drop table if exists `log_items`;
create table `log_items` (
    `item_id` int(11) not null primary key auto_increment,
    `item_name` varchar(256) not null unique,
    `item_q` int not null default 0
);

drop table if exists `log_reqs`;
create table `log_reqs` (
    `req_id` int(11) not null primary key auto_increment,
    `req_date` date not null,
    `cat_id` int(11),
    `item_id` int(11),
    `req_q` int not null,
    `req_status` ENUM('PENDING', 'ACCEPTED', 'REJECTED') not null default 'PENDING',
    `req_reason` text,
    foreign key (cat_id) references log_cats(cat_id) on delete cascade on update cascade,
    foreign key (item_id) references log_items(item_id) on delete cascade on update cascade
);
